#!/usr/bin/env python

# widgets.py - Shows some standard widgets.
#
# Copyright (C) 2007 David Boddie <david@boddie.org.uk>
#
# This file is part of Getting Started With PyQt4.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
from PyQt4.QtGui import *

app = QApplication(sys.argv)

checkbox = QCheckBox("C&ase sensitive")
combobox = QComboBox()
combobox.addItem("Large (L)")
spinbox = QSpinBox()

checkbox.show()
combobox.show()
spinbox.show()

sys.exit(app.exec_())

==========================
Getting Started With PyQt4
==========================

:Author: `David Boddie`_
:Date: 2011-03-26
:Version: 2

*Note: This text is marked up using reStructuredText formatting. It should be
readable in a text editor but can be processed to produce versions of this
document in other formats.*


.. contents::


Introduction
------------

This package contains slides and examples that aim to help beginners to PyQt4_
learn the basic principles behind the framework. They are intended to be read
by programmers who already have an understanding of the Python_ language.

The slides are provided in PDF format for convenience. The LaTeX_ sources and
images used to create the slides are provided. The examples presented in
the slides are included in the Examples directory.


Rebuilding the Slides
---------------------

The process of rebuilding the PDF file containing the slides requires pdfLaTeX_
and the `LaTeX Beamer`_ package. Other LaTeX packages that are also required
include `color` and `wrapfig`.

To generate the PDF file at the command line, enter the directory containing
the *Getting_Started_With_PyQt4.tex* file and invoke pdfLaTeX by typing::

  pdflatex Getting_Started_With_PyQt4.tex

You will need to do this twice to ensure that the table of contents is complete
and accurate.

If the slides are successfully created, something similar to the following text
will be written to the console, and you will be returned to the command line::

  Output written on Getting_Started_With_PyQt4.pdf (10 pages, 116023 bytes).
  Transcript written on Getting_Started_With_PyQt4.log.

If there is a problem during generation and an error is reported, you may be
shown a line of output containing question mark prompt like the following::

  ?

To return to the command line, simply type "X" and press Return. Type "?" and
press Return to get more information about the error.


Reporting Errors and Updating the Material
------------------------------------------

Please report any errors you find to me (`David Boddie`_). I hope to update the
slides and examples from time to time. If anyone else would like to contribute,
please get in touch and we can talk about setting up a public repository for
the material.


License
-------

The documents contained in this package, both in source form and generated, are
licensed under the GNU Free Documentation License (version 1.3 or later)::

  Copyright (C) 2009 David Boddie <david@boddie.org.uk>

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the COPYING.FDL.txt file.

The source code contained in this package is licensed under the GNU General
Public License (version 3 or later)::

 Getting Started With PyQt4 - An introduction to PyQt4 for Python programmers.
 Copyright (C) 2009 David Boddie <david@boddie.org.uk>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.



.. _PyQt4:                  http://www.riverbankcomputing.com/software/pyqt/
.. _Python:                 http://www.python.org/
.. _LaTeX:                  http://www.latex-project.org/
.. _pdfLaTeX:               http://www.pdftex.org
.. _`LaTeX Beamer`:         http://latex-beamer.sourceforge.net/
.. _`David Boddie`:         mailto:david@boddie.org.uk

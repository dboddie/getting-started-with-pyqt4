% Getting_Started_With_PyQt4.tex - An introduction to PyQt4 for Python programmers.
%
% This file is part of Getting Started With PyQt4.
%
% Copyright (C) 2007 David Boddie <david@boddie.org.uk>
%
% Permission is granted to copy, distribute and/or modify this document
% under the terms of the GNU Free Documentation License, Version 1.3
% or any later version published by the Free Software Foundation;
% with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
% A copy of the license is included in the section entitled "GNU
% Free Documentation License".

\documentclass{beamer}

\usepackage{color}
\usepackage{wrapfig}

\usepackage{beamerthemesplit}

\newcommand\fakelink[1]{%
    \textcolor{LinkColour}{#1}}

\title{Getting Started with PyQt4}
\author{David Boddie \\
        \fakelink{david@boddie.org.uk}}
\date{7\textsuperscript{th} February 2009}

\hypersetup{%
  pdftitle = {Getting Started with PyQt4},
  pdfkeywords = {PyQt},
  pdfauthor = {David Boddie},
  bookmarks=true,
  unicode=true
}

\newcommand\link[1]{%
   {\setlength{\parskip}{0pt}
   \normalfont\sffamily\color{blue}\href{#1}{#1}}}

\newcommand\namedlink[2]{%
   {\normalfont\sffamily\color{blue}\href{#1}{#2}}}

\newcommand\registered{%
   {\footnotesize\lower-.5ex\hbox{\textregistered}}}

\newcommand\trademark{%
   {\small\texttrademark}}

\newcommand\cpp{%
    C{\kern-1.25pt}\raisebox{1.75pt}{\fontsize{8}{8}\selectfont +{\kern-0.25pt}+}}

\newenvironment{legalese}
{%
\fontsize{7}{7}\selectfont
\setlength{\baselineskip}{9pt}
\setlength{\parindent}{0pt}
}
{}

\definecolor{TableHeaderColour}{cmyk}{0.3,0.1,0,0.1}
\definecolor{CodeBlockColour}{cmyk}{0.0,0.0,0.0,0.1}
\definecolor{BlockColour0}{cmyk}{0.3,0.1,0,0.05}
\definecolor{BlockColour1}{cmyk}{0.3,0.1,0.3,0.0}
\definecolor{ArgumentColour}{rgb}{0.6,0,0}
\definecolor{LinkColour}{rgb}{0,0,0.8}
\definecolor{BlueTheme}{rgb}{0.50,0.63,0.9}

% http://en.wikibooks.org/wiki/LaTeX/Customizing_LaTeX

\newenvironment{pythoncode}
{%
\fontsize{9}{9}\selectfont
\setlength{\baselineskip}{12pt}
\setlength{\parindent}{12pt}
\vspace*{0.25em}
\begin{tt}}
{%
\end{tt}
\vspace*{0.4em}}

% http://answers.google.com/answers/threadview?id=282787

\makeatletter\newenvironment{terminal}
{% Begin environment
   \vspace*{0.4em}
   \hspace*{8pt}
   \begin{lrbox}{\@tempboxa}\begin{minipage}[c]{\columnwidth - 32pt}
   \fontsize{9}{9}\selectfont\tt
   \setlength{\parindent}{0pt}
   \setlength{\baselineskip}{12pt}
   \textcolor{white}\bgroup
   \ignorespaces
}
{% End environment
   \egroup
   \end{minipage}\end{lrbox}%
   \colorbox{black}{\usebox{\@tempboxa}}
   \vspace*{0.4em}
}\makeatother

\newcommand\fakehtml[1]{%
    \fontsize{10}{10}\selectfont \setlength{\baselineskip}{12pt}{#1}}

\newcommand\qdoc[1]{%
    \textcolor{ArgumentColour}{$\backslash${#1}}}

\newcommand\argument[1]{%
    \textcolor{ArgumentColour}{#1}}

\definecolor{PythonCommentColour}{rgb}{0,0.4,0}
\definecolor{PythonKeywordColour}{rgb}{0.8,0.6,0}
\definecolor{PythonFunctionColour}{rgb}{0.0,0.0,0.8}
\definecolor{PythonDecoratorColour}{rgb}{0.4,0.4,0.8}
\definecolor{PythonClassColour}{rgb}{0.0,0.4,0.8}
\definecolor{PythonStringColour}{rgb}{0.0,0.0,0.6}
\definecolor{PythonOperatorColour}{rgb}{0.6,0.6,0}

\newcommand\pycomment[1]{%
    \textcolor{PythonCommentColour}{#1}}
\newcommand\pykeyword[1]{%
    \textcolor{PythonKeywordColour}{#1}}
\newcommand\pycallable[1]{%
    \textcolor{PythonFunctionColour}{#1}}
\newcommand\pydecorator[1]{%
    \textcolor{PythonDecoratorColour}{#1}}
\newcommand\pyclass[1]{%
    \textcolor{PythonClassColour}{#1}}
\newcommand\pystring[1]{%
    \textcolor{PythonStringColour}{#1}}
\newcommand\pyoperator[1]{%
    \textcolor{PythonOperatorColour}{#1}}

\newcommand\pyprompt{%
    {\fontsize{8}{8}\selectfont $>>>$~}}
\newcommand\pypromptdots{%
    {\fontsize{8}{8}\selectfont .\hspace*{3.5pt}.\hspace*{3.5pt}.~}}

\providecommand{\LyX}{L\kern-.1667em\lower.25em\hbox{Y}\kern-.125emX\@}

\mode<presentation>{
\usecolortheme[named=BlueTheme]{structure}
}

\begin{document}

\frame{\titlepage}

\frame{
\frametitle{Copyright and License Information}

\begin{legalese}
Copyright (C) 2007 David Boddie $<$david$@$boddie.org.uk$>$
\vspace{0.5em}

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the COPYING.FDL.txt file that is
supplied in the package containing the source material for this document.
\end{legalese}
}

\section[Outline]{}
\frame{\tableofcontents}

\section{Overview}

\frame{
\frametitle{Overview}
\namedlink{http://www.riverbankcomputing.com/}{PyQt} is a set of Python
bindings to the \namedlink{http://trolltech.com/}{Qt} application framework,
providing access to features which include:

\begin{itemize}
\item Widgets and other graphical user interface controls
\item Database management and querying
\item XML handling and processing
\item Graphics and multimedia
\item Web browser integration and networking
\end{itemize}

PyQt is available under the
\namedlink{http://www.gnu.org/licenses/gpl-3.0.html}{GNU General Public License}.
}

\section{Installing PyQt}

\frame{
\frametitle{Installing Qt and PyQt}

Both Qt and PyQt are available as standard in many GNU/Linux distributions:
\begin{itemize}
\item \textbf{Debian, Ubuntu:} \texttt{python-qt4} and \texttt{pyqt4-dev-tools}
\item \textbf{openSUSE:} \texttt{python-qt4}
\item \textbf{Mandriva:} \texttt{python-qt4}
\item \textbf{Fedora:} \texttt{PyQt4}
\item \textbf{Pardus:} \texttt{PyQt4}
\end{itemize}

% Debian etch (4.0.1), lenny (4.4.2), sid (4.4.2), experimental (4.4.2, 4.4.3)
% Feisty (4.1), Gutsy (4.3), Hardy (4.3.3), Intrepid (4.4.3), Jaunty (4.4.3)
% openSUSE 11.0 (4.4.2)
% Mandriva 2008.1 (4.3.3)
% Fedora 8 (4.2), 9 (4.3.3), 10 (4.4.3)
% Pardus 2007 (4.4.2), 2008 (4.4.4)

The \namedlink{http://www.riverbankcomputing.com/software/pyqt/download}{PyQt
download page} contains:

\begin{itemize}
\item Binary installers for Windows
\item Source packages for Mac OS X, Windows, Unix and GNU/Linux
\end{itemize}
}

\subsection{Checking the Installation}

\frame{
\frametitle{Checking the Installation}

Start a Python session in a terminal and type:

% Examples/Checking_the_Installation/main.py
\begin{pythoncode}
\pykeyword{from}~PyQt4.QtCore \pykeyword{import}~QT\_VERSION\_STR\\
\pykeyword{print}~QT\_VERSION\_STR\\
\end{pythoncode}

The version of Qt in use should be printed to the terminal.

\begin{terminal}
\pyprompt from PyQt4.QtCore import QT\_VERSION\_STR\\
\pyprompt print QT\_VERSION\_STR\\
4.3.2
\end{terminal}

The version reported will depend on your PyQt installation.
}

\section{Hello World in PyQt}

\frame{
\frametitle{Hello World in PyQt}

Here's a simple PyQt application:

% Examples/Hello_World/hello.py
\begin{pythoncode}
\pykeyword{import}~sys\\
\pykeyword{from}~PyQt4.QtGui \pykeyword{import}~\pyclass{QApplication}, \pyclass{QPushButton}\\
\vspace*{0.25em}
app \pyoperator{=}~\pyclass{\pycallable{QApplication}}(sys.argv)\\
button \pyoperator{=}~\pyclass{\pycallable{QPushButton}}(\pystring{"Hello world!"})\\
button.\pycallable{show}()\\
sys.\pycallable{exit}(app.\pycallable{exec\_}())\\
\end{pythoncode}

\begin{minipage}[m]{3.5cm}
This does four things:
\end{minipage}
\hspace*{0.25cm}
\begin{minipage}[m]{6.5cm}
\begin{enumerate}
\item Creates an application object
\item Creates a button
\item Shows the button
\item Runs the event loop
\end{enumerate}
\end{minipage}
}

\subsection{PyQt Concepts -- The Event Loop}

\frame{
\frametitle{PyQt Concepts -- The Event Loop}

The application communicates with the system via events.

\begin{itemize}
\item We do not dispatch events ourselves.
\item We ask the \pyclass{QApplication} object to run an event loop.
\item The event loop controls the execution of the application.
\item We give up control when it starts running, but it calls functions
and methods that we provide.
\end{itemize}

The application won't work unless we start an event loop by calling
the \pyclass{QApplication} object's \pycallable{exec\_()} method.
}

\section{Widgets}

\frame{
\frametitle{Widgets}

Widgets are graphical elements that the user interacts with.

% Examples/Widgets/widgets.py
\begin{pythoncode}
checkbox \pyoperator{=}~\pyclass{\pycallable{QCheckBox}}(\pystring{"C\&ase sensitive"})\\
combobox \pyoperator{=}~\pyclass{\pycallable{QComboBox}}()\\
combobox.\pycallable{addItem}(\pystring{"Large (L)"})\\
spinbox \pyoperator{=}~\pyclass{\pycallable{QSpinBox}}()\\
\end{pythoncode}

\begin{center}
\begin{minipage}[t]{3cm}
\setlength{\baselineskip}{12pt}
\begin{center}
\includegraphics[scale=0.48, keepaspectratio]{Images/checkbox.png}\\
\pyclass{QCheckBox}
\end{center}
\end{minipage}
\hspace*{12pt}
\begin{minipage}[t]{3cm}
\setlength{\baselineskip}{12pt}
\begin{center}
\includegraphics[scale=0.48, keepaspectratio]{Images/combobox.png}\\
\pyclass{QComboBox}
\end{center}
\end{minipage}
\hspace*{12pt}
\begin{minipage}[t]{3cm}
\setlength{\baselineskip}{12pt}
\begin{center}
\includegraphics[scale=0.48, keepaspectratio]{Images/spinbox.png}\\
\pyclass{QSpinBox}
\end{center}
\end{minipage}
\end{center}
}

\subsection{Widgets and Layouts}

\frame{
\frametitle{Widgets and Layouts}

Widgets can be used as containers to hold other widgets.

\begin{wrapfigure}{r}{0.3\textwidth}
\begin{center}
\includegraphics[scale=0.48, keepaspectratio]{Images/layout.png}
\end{center}
\vspace*{-0.5cm}
\end{wrapfigure}

% Examples/Widgets_and_Layouts/layouts.py
\begin{pythoncode}
window \pyoperator{=}~\pyclass{\pycallable{QWidget}}()\\
label \pyoperator{=}~\pyclass{\pycallable{QLabel}}(\pystring{"Name:"})\\
editor \pyoperator{=}~\pyclass{\pycallable{QLineEdit}}()\\
\vspace*{0.25em}
layout \pyoperator{=}~\pyclass{\pycallable{QHBoxLayout}}(window)\\
layout.\pycallable{addWidget}(label)\\
layout.\pycallable{addWidget}(editor)\\
\vspace*{0.25em}
window.\pycallable{show}()\\
\end{pythoncode}

\begin{itemize}
\item The label, line edit and window are created in the same way.
\item The layout puts the label and line edit inside the widget.
\item The window is the parent of both the label and line edit.
\end{itemize}
}

\subsection{PyQt Concepts -- Parents and Children}

\frame{
\frametitle{PyQt Concepts -- Parents and Children}

Container widgets are parents of the widgets inside them:

\begin{itemize}
\item Windows have no parents.
\item Child widgets can have their own children.
\item Children of a widget are only visible if their parent is.
\end{itemize}

Parent widgets ``own'' their children:

\begin{pythoncode}
def create\_window():\\
~~~~window \pyoperator{=}~\pyclass{\pycallable{QWidget}}()\\
~~~~editor \pyoperator{=}~\pyclass{\pycallable{QLineEdit}}()\\
~~~~layout \pyoperator{=}~\pyclass{\pycallable{QHBoxLayout}}(window)\\
~~~~layout.\pycallable{addWidget}(editor)\\
~~~~return window
\end{pythoncode}

The editor and layout objects go out of scope but are not deleted.
}
\end{document}
